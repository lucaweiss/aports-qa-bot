// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"encoding/json"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/client"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/conf"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/server"
)

func main() {
	// Make it pretty-print for Console
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339})

	file, err := os.Open("conf.json")
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("failed to read configuration file")
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	config := conf.Options{}
	err = decoder.Decode(&config)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("failed to decode configuration")
	}

	// Set the zerolog logging to this configuration level
	level, err := zerolog.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("loglevel", config.LogLevel).
			Msg("LogLevel in configuration is invalid")
	}
	zerolog.SetGlobalLevel(level)
	log.Info().
		Str("loglevel", level.String()).
		Msg("set loglevel from configuration")

	overrideFile, err := os.Open("conf.override.json")
	if err == nil {
		defer overrideFile.Close()
		decoder := json.NewDecoder(overrideFile)
		err = decoder.Decode(&config)
		if err != nil {
			log.Fatal().
				Err(err).
				Msg("failed to decode configuration")
		}
	} else {
		log.Warn().
			Err(err).
			Msg("failed to read override configuration, continuing with values in conf.json")
	}

	webhookServer, sErr := server.NewWebhookEventListener(&config)
	if sErr != nil {
		log.Fatal().
			Err(sErr).
			Msg("failed to start server")
	}
	pollClient, cErr := client.NewClient(&config)
	if cErr != nil {
		log.Fatal().
			Err(cErr).
			Msg("failed to launch client")
	}
	defer pollClient.Close()
	pollClient.Poll()
	log.Fatal().Err(webhookServer.Listen())
}
