// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
// SPDX-FileCopyrightText: 2021 Leo <thinakbit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package server

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"sync"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/conf"
)

// The WebhookEventListener server, which listens for notifications from Gitlab
type WebhookEventListener struct {
	config             *conf.Options
	client             *gitlab.Client
	registeredServices []Services.WebHookService
}

// NewWebhookEventListener creates a new WebhookEventListener and registers a HTTP handler for /triage/system-hooks.
// To actually listen for webhooks you still have to call WebhookEventListener.Listen()
func NewWebhookEventListener(config *conf.Options) (*WebhookEventListener, error) {
	l := new(WebhookEventListener)
	l.config = config
	client, err := gitlab.NewClient(
		config.APIToken,
		gitlab.WithBaseURL(config.ProxyURL),
	)
	if err != nil {
		return nil, err
	}

	l.client = client

	l.registeredServices = Services.RegisterWebHookServices(client, &config.Services.WebHook)

	return l, nil
}

// Listen listens for HTTP requests on the port specified via the WebhookEventListener's config.
// This function blocks until an error occurs (e.g. if we can't bind to the port)
func (l *WebhookEventListener) Listen() error {
	http.Handle("/triage/system-hooks", http.HandlerFunc(l.HookHandler))
	return http.ListenAndServe(fmt.Sprintf(":%d", l.config.ServerPort), nil)
}

// HookHandler handles HTTP requests, checks for the appropriate Gitlab token and may create a new Job for
// doing certain actions at Gitlab in response.
func (l *WebhookEventListener) HookHandler(w http.ResponseWriter, req *http.Request) {
	log.Info().Msg("handling request...")

	w.Header().Set("Content-Type", "application/json")

	if req.Header.Get("X-Gitlab-Token") != l.config.WebhookToken {
		log.Error().
			Err(errors.New("invalid token")).
			Str("component", "WebHookHandler").
			Str("token", req.Header.Get("X-Gitlab-Token")).
			Msg("Hook-Notification failed")
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "{\"message\":\"FAIL\"}")
		return
	}

	// Return an http response as fast as possible, we should not exceed the
	// configured timeout according to:
	// https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#configure-your-webhook-receiver-endpoint
	// We also return 201 instead of 200 to indicate that our WebHook is asynchronous
	w.WriteHeader(http.StatusCreated)

	defer req.Body.Close()

	body, _ := io.ReadAll(req.Body)

	eventType := gitlab.HookEventType(req)
	event, err := gitlab.ParseWebhook(eventType, body)
	if err != nil {
		log.Error().
			Err(err).
			Str("component", "WebHookHandler").
			Str("token", req.Header.Get("X-Gitlab-Token")).
			Msg("could not parse WebHook")
		return
	}

	log.Info().
		Str("component", "WebHookHandler").
		Str("event", string(eventType)).
		Msg("received event")

	switch eventContent := event.(type) {
	// Contrary to what the name implies this is actually an event related to a MERGE REQUEST
	// and not an event that is triggered when a merge request is merged
	case *gitlab.MergeEvent:
		processMergeRequestEvent(eventContent, l.client, l.registeredServices)
	default:
		log.Warn().
			Str("event", string(eventType)).
			Str("component", "WebHookHandler").
			Msg("Got unknown Gitlab event")
	}
}

// processMergeRequestEvent processes a gitlab.MergeEvent and may call Gitlab's API (e.g. to enable collaboration on new MRs)
func processMergeRequestEvent(payload *gitlab.MergeEvent, gitlabClient *gitlab.Client, registeredServices []Services.WebHookService) {
	MergeRequestJobID := uuid.New()

	// Create a new sublogger that has associated fields, this is passed to any service functions
	// as `log`
	log := log.With().
		Str("component", "MergeRequestJob processor").
		Int("MR", payload.ObjectAttributes.IID).
		Int("project", payload.ObjectAttributes.TargetProjectID).
		Str("uuid", MergeRequestJobID.String()).
		Logger()

	log.Info().
		Str("action", payload.ObjectAttributes.Action).
		Str("state", payload.ObjectAttributes.State).
		Msg("processing")

	// Validate that the Action and State we got are inside our expected
	// values
	state, stateErr := MergeRequest.StateString(payload.ObjectAttributes.State)
	if stateErr != nil {
		log.Warn().
			Err(stateErr).
			Str("state", payload.ObjectAttributes.State).
			Msg("unimplemented state")
	}

	action, actionErr := MergeRequest.ActionString(payload.ObjectAttributes.Action)
	if actionErr != nil {
		log.Warn().
			Err(actionErr).
			Str("action", payload.ObjectAttributes.Action).
			Msg("unimplemented action")
	}

	if stateErr == nil && actionErr == nil {
		var wg sync.WaitGroup

		// Length of registeredServices is how many goroutines we will run
		wg.Add(len(registeredServices))

		for i := range registeredServices {
			// Run each iteration of the loop inside a an anonymous function
			// this decouples the task of doing `wg.Done()` from the structures
			// and also encapsulates the `getActions()` and `getStates()` from being
			// synchronous
			go func(i int) {
				defer wg.Done()
				actionMap := registeredServices[i].GetActions()
				stateMap := registeredServices[i].GetStates()

				if actionMap[action] && stateMap[state] {
					registeredServices[i].Process(payload, &log)
				}
			}(i)
		}

		// Wait for all services to finish before logging that the
		// merge request has been `processed`
		wg.Wait()
	}

	log.Info().
		Str("action", payload.ObjectAttributes.Action).
		Str("state", payload.ObjectAttributes.State).
		Msg("processed")
}
