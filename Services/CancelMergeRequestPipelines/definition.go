// SPDX-FileCopyrightText: 2021 Rasmus Thomsen <oss@cogitri.dev>
// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package CancelMergeRequestPipelines

import (
	"github.com/rs/zerolog"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
)

type Service struct {
	dryRun       bool
	gitlabClient *gitlab.Client
}

func New(dryRun bool, gitlabClient *gitlab.Client) Service {
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
	}
}

var actions = map[MergeRequest.Action]bool{
	MergeRequest.Close: true,
	MergeRequest.Merge: true,
}

var states = map[MergeRequest.State]bool{
	MergeRequest.Closed: true,
	MergeRequest.Merged: true,
}

func (Service) GetActions() map[MergeRequest.Action]bool {
	return actions
}

func (Service) GetStates() map[MergeRequest.State]bool {
	return states
}

func (s Service) Process(payload *gitlab.MergeEvent, log *zerolog.Logger) {
	// Create a logger we can use with all the information we need
	sLog := log.With().
		Str("service", "CancelMergeRequestsPipeline").
		Logger()

	sLog.Info().Msg("starting")

	res, _, err := s.gitlabClient.MergeRequests.ListMergeRequestPipelines(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
	)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed to get list of pipelines")

		return
	}

	resMR, _, errMR := s.gitlabClient.MergeRequests.GetMergeRequest(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
		&gitlab.GetMergeRequestsOptions{},
	)
	if errMR != nil {
		sLog.Error().
			Err(errMR).
			Msg("failed to get MR info")

		return
	}

	for _, pipeline := range res {
		sLog.Info().
			Int("Pipeline", pipeline.ID).
			Msg("canceling pipeline")

		if !s.dryRun {
			// We have no way of knowing whether the MR was created by someone with push permissions (in which case it's created in the upstream repo)
			// or in the downstream repo (if someone w/o push permissions caused the event, e.g. by closing the MR.)
			_, _, upstreamErr := s.gitlabClient.Pipelines.CancelPipelineBuild(payload.ObjectAttributes.TargetProjectID, pipeline.ID)
			_, _, verr := s.gitlabClient.Pipelines.CancelPipelineBuild(resMR.SourceProjectID, pipeline.ID)
			if verr != nil && upstreamErr != nil {
				sLog.Error().
					Err(verr).
					Err(upstreamErr).
					Int("pipeline", pipeline.ID).
					Msg("couldn't cancel pipeline")
				return
			}
		}
	}

	sLog.Info().Msg("finished")
}
