// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
// SPDX-FileCopyrightText: 2021 Leo <thinakbit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package MergeRequest

// Action enumerates the different actions users can do to a merge request
type Action int

//go:generate go run github.com/dmarkham/enumer -type=Action -transform=lower
const (
	Close    Action = iota // Close means that the MR was closed
	Open                   // Open means that MR was created
	Merge                  // Merge means the MR was merged
	Reopen                 // Reopen means that MR was opened again
	Update                 // Update means that MR was updated
	Approved               // Approve means that MR was approve
)
