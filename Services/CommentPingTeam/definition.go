// SPDX-FileCopyrightText: 2022 Pablo Correa Gomez <ablocorrea@hotmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package CommentPingTeam

import (
	"fmt"
	"strings"

	"github.com/rs/zerolog"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services/AutoMaintainer"
)

const noteTemplate = "\U0001F3D3 @%s"

type Service struct {
	dryRun       bool
	gitlabClient *gitlab.Client
	pingTeams    []string
}

func New(dryRun bool, gitlabClient *gitlab.Client, pingTeams []string) Service {
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
		pingTeams:    pingTeams,
	}
}

var actions = map[MergeRequest.Action]bool{
	MergeRequest.Open: true,
}

var states = map[MergeRequest.State]bool{
	MergeRequest.Opened: true,
}

func (Service) GetActions() map[MergeRequest.Action]bool {
	return actions
}

func (Service) GetStates() map[MergeRequest.State]bool {
	return states
}

func (s Service) Process(payload *gitlab.MergeEvent, log *zerolog.Logger) {
	// Create a logger we can use with all the information we need
	sLog := log.With().
		Str("service", "CommentPingTeam").
		Logger()

	sLog.Info().Msg("starting")

	res, _, err := s.gitlabClient.MergeRequests.GetMergeRequestChanges(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
		&gitlab.GetMergeRequestChangesOptions{},
	)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed")
		return
	}

	// List of maintainers that have been found
	maintainersFound := make(map[string]bool)

	for _, change := range res.Changes {
		// Skip over if the change is not an APKBUILD
		if !strings.Contains(change.NewPath, "/APKBUILD") {
			continue
		}

		// Search for the Team in the target repository unless it is a
		// new file. In such case, ping the team to let them know that
		// there is a new package under their maintenance
		projectID := payload.ObjectAttributes.TargetProjectID
		path := change.OldPath
		branch := payload.ObjectAttributes.TargetBranch
		if change.NewFile {
			projectID = payload.ObjectAttributes.SourceProjectID
			path = change.NewPath
			branch = payload.ObjectAttributes.SourceBranch
		}

		var maintainerSlice *[2]AutoMaintainer.Maintainer
		maintainerSlice, err = AutoMaintainer.ExtractMaintainer(
			s.gitlabClient,
			projectID,
			path,
			branch,
		)
		if err != nil {
			sLog.Warn().
				Err(err).
				Str("file", path).
				Str("reference", branch).
				Msg("could not extract maintainer name")
			continue
		}

		for _, maintainer := range maintainerSlice {
			maintainersFound[maintainer.GetName()] = true
		}
	}

	if len(maintainersFound) == 0 {
		sLog.Warn().Msg("no maintainers found")
		return
	}

	var teams []string
	for k := range maintainersFound {
		for _, t := range s.pingTeams {
			if strings.EqualFold(k, t) {
				teams = append(teams, t)
			}
		}
	}

	if len(teams) == 0 {
		sLog.Warn().Msg("no teams found to ping")
		return
	}

	if !s.dryRun {
		for _, t := range teams {
			noteText := fmt.Sprintf(noteTemplate, t)
			_, _, err = s.gitlabClient.Notes.CreateMergeRequestNote(
				payload.ObjectAttributes.TargetProjectID,
				payload.ObjectAttributes.IID,
				&gitlab.CreateMergeRequestNoteOptions{Body: gitlab.String(noteText)},
			)
			if err != nil {
				sLog.Error().
					Err(err).
					Msg("failed to ping team")
				return
			}
		}
	}

	sLog.Info().Msg("finished")
}
