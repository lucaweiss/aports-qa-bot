// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
// SPDX-FileCopyrightText: 2021 Leo <thinakbit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package MergeRequest

import "github.com/xanzy/go-gitlab"

// ContainsLabel is given a string and a list and checks if the string is in the List
func ContainsLabel(needle string, haystack []*gitlab.Label) bool {
	for _, i := range haystack {
		if i.Name == needle {
			return true
		}
	}
	return false
}
