// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
// SPDX-FileCopyrightText: 2021 Leo <thinakbit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package MergeRequest

// State enumerates the different states a MR can be in
type State int

//go:generate go run github.com/dmarkham/enumer -type=State -transform=lower
const (
	Closed State = iota // Closed means that MR has been closed
	Locked              // Locked means that MR has been locked
	Merged              // Merged means that MR has been merged
	Opened              // Opened means that MR has been opened
)
