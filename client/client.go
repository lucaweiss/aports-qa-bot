// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package client

import (
	"github.com/google/uuid"
	"github.com/robfig/cron/v3"
	"github.com/rs/zerolog/log"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/Services"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/conf"
)

// The Client polls the Gitlab API periodically
type Client struct {
	Config       *conf.Options
	Cron         *cron.Cron
	CronEntryID  cron.EntryID
	GitlabClient *gitlab.Client
	Services     []Services.PollerService
}

// NewClient creates a new Client and adds a cron for polling as often as config.PollerCron says
func NewClient(config *conf.Options) (*Client, error) {
	client := new(Client)
	client.Config = config
	client.Cron = cron.New()

	gitlabClient, err := gitlab.NewClient(
		config.APIToken,
		gitlab.WithBaseURL(config.ProxyURL),
	)
	if err != nil {
		return nil, err
	}

	client.GitlabClient = gitlabClient

	entryID, err := client.Cron.AddFunc(config.PollerCron, client.Poll)
	if err != nil {
		return nil, err
	}

	client.Cron.Start()

	client.CronEntryID = entryID

	client.Services = Services.RegisterPollerServices(
		client.GitlabClient,
		client.Config.Services.Poller,
	)

	return client, nil
}

// Close stops the client from running any more poll jobs
func (c *Client) Close() {
	c.Cron.Remove(c.CronEntryID)
}

// Poll polls the Gitlab API for new events
func (c *Client) Poll() {
	MergeRequestJobID := uuid.New()

	log := log.With().
		Str("component", "cron").
		Str("uuid", MergeRequestJobID.String()).
		Logger()

	log.Info().
		Msg("Polling for changes..")

	for _, pid := range c.Config.PollerIDs {
		sLog := log.With().
			Int("pid", pid).
			Logger()

		for _, service := range c.Services {
			service.Process(pid, &sLog)
		}
	}
}
