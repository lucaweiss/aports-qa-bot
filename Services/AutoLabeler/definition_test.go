// SPDX-FileCopyrightText: 2020-2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package AutoLabeler

import (
	"math/rand"
	"strconv"
	"testing"
	"time"
)

// Taken from https://gitlab.alpinelinux.org/api/v4/projects/1/merge_requests/14486/changes
// specifically the diff field of the changes field
const (
	HasUpgradeJSON = "@@ -1,7 +1,7 @@\n # Maintainer: Natanael Copa \u003cncopa@alpinelinux.org\u003e\n pkgname=sshfs\n-pkgver=3.7.0\n-pkgrel=4\n+pkgver=3.7.1\n+pkgrel=0\n pkgdesc=\"FUSE client based on the SSH File Transfer Protocol\"\n url=\"https://github.com/libfuse/sshfs\"\n arch=\"all\"\n@@ -25,4 +25,4 @@ package() {\n \t\t\"$pkgdir\"/usr/share/doc/$pkgname/\n }\n \n-sha512sums=\"bd8bcd45dd9a5e9686c6fb442e877ffdb592ba0d3424d5dab84a955bfafb17e8666abefba6857467833f5b285842bdadd5a9b6c9e8128ac2e564c36df79aa570  sshfs-3.7.0.tar.xz\"\n+sha512sums=\"ee91b2eacbad5891006dbac14188ddd591e242b6092c7b7d8234503d79acb52f4b7ea9a15d5eaad83597ff4b4e700580ee2271671edfa6db762d9a8c756d45fd  sshfs-3.7.1.tar.xz\"\n"
	NoUpgradeJSON  = "@@ -18,7 +18,7 @@ builddir=\"$srcdir/$_pkgname-$pkgver\"\n build() {\n \tmkdir -p \"$builddir\"/build\n \tcd \"$builddir\"/build\n-\texport CXXFLAGS=\"$CXXFLAGS -Wno-error=deprecated-copy\"\n+\texport CXXFLAGS=\"$CXXFLAGS -Wno-error=deprecated-copy -Wno-error=type-limits\"\n \tcmake \\\n \t\t-DCMAKE_BUILD_TYPE=None \\\n \t\t-DCASS_BUILD_STATIC=ON \\\n"
)

// assertTrue checks whether the value we expected
func assertBool(t *testing.T, expected, got bool) {
	t.Helper()
	if expected != got {
		t.Errorf("Expected '%v', got '%t'", expected, got)
	}
}

func TestIsUpgrade(t *testing.T) {
	assertBool(t, true, isUpgrade(HasUpgradeJSON))
	assertBool(t, false, isUpgrade(NoUpgradeJSON))
}

func TestGoodPaths(t *testing.T) {
	_, err := ParsePath("community/libdrm/APKBUILD")
	if err != nil {
		t.Errorf("unexpected error %v", err)
	}
}

func TestPathHasAPKBUILD(t *testing.T) {
	r, err := ParsePath("community/libdrm/APKBUILD")
	if err != nil {
		t.Errorf("unexpected error %v", err)
	}
	if !r.IsAPKBUILD {
		t.Errorf("Expected r.APKBUILD to be true but it is false")
	}
}

func TestPathDoesNotHaveAPKBUILD(t *testing.T) {
	r, err := ParsePath("community/libdrm/fix-gcc-10.patch")
	if err != nil {
		t.Errorf("unexpected error %v", err)
	}
	if r.IsAPKBUILD {
		t.Errorf("Expected r.APKBUILD to be false but it is true")
	}
}

func assertTooShortError(t *testing.T, err error) {
	if err == nil {
		t.Errorf("Expected 'not enough elements' error but got none")
	}
	if err.Error() != "not enough elements" {
		t.Errorf("unexpected error %v", err)
	}
}

func TestPathIsTooShort(t *testing.T) {
	for _, s := range []string{"", "/", "//", "///"} {
		t.Run(s+"community/"+s+"libdrm"+s, func(t *testing.T) {
			_, err := ParsePath(s + "community/" + s + "libdrm" + s)
			assertTooShortError(t, err)
		})
	}
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%*¨&*()_-=+{}[];ç"

func randStringBytesRemainder(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))]
	}
	return string(b)
}

func TestInvalidRepoPath(t *testing.T) {
	for i := 1; i < 256; i++ {
		for j := 0; j < 10; j++ {
			rand.Seed(time.Now().UnixNano())
			baseRandomString := randStringBytesRemainder(i)
			FormattedRandomString := baseRandomString + "/alpine-sdk" + "/APKBUILD"
			// Skip the test if it has a valid name we know of
			if contains(FormattedRandomString, validRepoNames) {
				continue
			}
			t.Run("With length "+strconv.Itoa(i)+" and named "+baseRandomString, func(t *testing.T) {
				_, err := ParsePath(FormattedRandomString)
				if err == nil {
					t.Error("Expected 'repository name isn't valid' error but got none")
				}
				if err.Error() != "repository name isn't valid" {
					t.Errorf("unexpected error %v", err)
				}
			})
		}
	}
}
