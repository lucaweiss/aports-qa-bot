// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package AutoMaintainer

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"net/mail"
	"regexp"
	"strings"

	"github.com/rs/zerolog"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
)

type Maintainer struct {
	name  string
	email string
}

func (m *Maintainer) GetName() string {
	return m.name
}

func (m *Maintainer) GetEmail() string {
	return m.email
}

// hasUser takes an email address and searches in the gitlab database for it
//
// It will return an error if:
//   - There is more than 1 user with a matching email address
//   - There are no users with the matching email address
//   - The request to GitLab has failed
//
// If only one user is found then it is returned
func hasUser(gitlabClient *gitlab.Client, email string) (*gitlab.User, error) {
	users, _, err := gitlabClient.Users.ListUsers(
		&gitlab.ListUsersOptions{
			Search: &email,
		},
	)
	if err != nil {
		return nil, err
	}
	if len(users) > 1 {
		return nil, fmt.Errorf("too many users found with email address %q", email)
	}
	if len(users) == 0 {
		return nil, fmt.Errorf("no users found with email address %q", email)
	}
	return users[0], nil
}

// ExtractMaintainer takes a ProjectID and a filename and tries to extract the user name
// and email address of the '# Maintainer:' field
func ExtractMaintainer(gitlabClient *gitlab.Client, projectID interface{}, filename, ref string) (*[2]Maintainer, error) {
	maintainerSlice := [2]Maintainer{}

	file, _, err := gitlabClient.RepositoryFiles.GetRawFile(
		projectID,
		filename,
		&gitlab.GetRawFileOptions{Ref: gitlab.String(ref)},
	)
	if err != nil {
		return nil, err
	}

	// Run over every line of the received raw file, we will check the first
	// occurrence of '# Maintainer'
	scanner := bufio.NewScanner(bytes.NewReader(file))
	for scanner.Scan() {
		scannedLine := scanner.Text()

		if strings.HasPrefix(scannedLine, "# Maintainer:") {
			// Strip the # Maintainer: and pass everything else
			line := strings.ReplaceAll(scannedLine, "# Maintainer:", "")
			line = strings.TrimSpace(line)

			address, err := mail.ParseAddress(line)
			if err != nil {
				return nil, err
			}
			// Store a full and a stripped version
			maintainerSlice[0] = Maintainer{name: address.Name, email: address.Address}

			// Strip the +tag so we get the true address
			re := regexp.MustCompile(`\+.+@`)
			strippedEmail := re.ReplaceAllString(address.Address, "@")
			maintainerSlice[1] = Maintainer{name: address.Name, email: strippedEmail}

			return &maintainerSlice, nil
		}
	}
	return nil, errors.New("no maintainer field")
}

type Service struct {
	dryRun       bool
	gitlabClient *gitlab.Client
}

var actions = map[MergeRequest.Action]bool{
	MergeRequest.Reopen: true,
	MergeRequest.Open:   true,
}

var states = map[MergeRequest.State]bool{
	MergeRequest.Opened: true,
}

func New(dryRun bool, gitlabClient *gitlab.Client) Service {
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
	}
}

func (Service) GetActions() map[MergeRequest.Action]bool {
	return actions
}

func (Service) GetStates() map[MergeRequest.State]bool {
	return states
}

// Process processes a *gitlab.MergeEvent to automatically add a maintainer to a merge request
func (s Service) Process(payload *gitlab.MergeEvent, log *zerolog.Logger) {
	// Create a logger we can use with all the information we need
	sLog := log.With().
		Str("service", "AutoMaintainer").
		Logger()

	sLog.Info().Msg("starting")

	if len(payload.ObjectAttributes.AssigneeIDs) != 0 {
		sLog.Info().Msg("finished")
		return
	}

	res, _, err := s.gitlabClient.MergeRequests.GetMergeRequestChanges(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
		&gitlab.GetMergeRequestChangesOptions{},
	)
	if err != nil {
		sLog.Error().
			Err(err).
			Msg("failed")
		return
	}

	// List of maintainers that have been found
	maintainersFound := make(map[string]bool)

	for _, change := range res.Changes {
		// Skip over changes that add new files
		if change.NewFile {
			continue
		}
		// Skip over if the change is not an APKBUILD
		if !strings.Contains(change.NewPath, "/APKBUILD") {
			continue
		}

		maintainerSlice, err := ExtractMaintainer(
			s.gitlabClient,
			payload.ObjectAttributes.TargetProjectID,
			change.OldPath, // Use OldPath because we query the repository which doesn't have the changes
			payload.ObjectAttributes.TargetBranch,
		)
		if err != nil {
			sLog.Warn().
				Err(err).
				Str("file", change.OldPath).
				Str("reference", payload.ObjectAttributes.TargetBranch).
				Msg("could not extract maintainer email")
			continue
		}
		for _, maintainer := range maintainerSlice {
			// Add the maintainerEmail we found from the APKBUILD into the map
			maintainersFound[maintainer.GetEmail()] = true
		}
	}

	if len(maintainersFound) == 0 {
		sLog.Warn().Msg("no maintainers found to assign")
		// TODO: remove maintainers once we are confident enough that there are no false positives
		// we don't want to be removing maintainers automatically just because aports-qa-bot is not
		// smart enough to connect maintainers to their email addresses
		return
	}

	if len(maintainersFound) > 1 {
		sLog.Warn().Msg("too many maintainers found to assign")
		return
	}

	for k := range maintainersFound {
		// Transform the email into the user
		maintainerUser, err := hasUser(s.gitlabClient, k)
		if err != nil {
			sLog.Error().
				Err(err).
				Msg("failed to convert APKBUILD maintainer to GitLab user")

			return
		}

		// Try to get Username via its ID
		author, _, err := s.gitlabClient.Users.GetUser(
			payload.ObjectAttributes.AuthorID,
			gitlab.GetUsersOptions{},
		)
		if err != nil {
			sLog.Error().
				Err(err).
				Msg("failed to convert MR author to GitLab user")
			return
		}

		if maintainerUser.Username == author.Username {
			// We use `info` instead of `error` here because the Author of the Merge Request
			// and the APKBUILD maintainer being the same person is an expected and very common
			// situation
			sLog.Info().Msg("MR author and APKBUILD maintainer are the same user")
			return
		}

		sLog.Info().Str("user", maintainerUser.Username).Msg("assigning")

		if !s.dryRun {
			_, _, err = s.gitlabClient.MergeRequests.UpdateMergeRequest(
				payload.ObjectAttributes.TargetProjectID,
				payload.ObjectAttributes.IID,
				&gitlab.UpdateMergeRequestOptions{
					AssigneeID: gitlab.Int(maintainerUser.ID),
				},
			)
			if err != nil {
				sLog.Error().
					Err(err).
					Str("user", maintainerUser.Username).
					Msg("failed to assign")
				return
			}
		}
	}

	sLog.Info().Msg("finished")
}
